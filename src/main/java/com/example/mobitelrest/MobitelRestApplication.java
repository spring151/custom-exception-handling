package com.example.mobitelrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com")
public class MobitelRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobitelRestApplication.class, args);
	}

}
