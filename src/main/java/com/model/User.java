package com.model;

public class User {
	private String uname;
	private String password;
	private String email;
	private String city;
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPassword() {
		return password;
	}
	@Override
	public String toString() {
		return "User [uname=" + uname + ", password=" + password + ", email=" + email + ", city=" + city + "]";
	}
	public User(String uname, String password, String email, String city) {
		super();
		this.uname = uname;
		this.password = password;
		this.email = email;
		this.city = city;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}