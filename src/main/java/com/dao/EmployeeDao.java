package com.dao;

import org.springframework.stereotype.Service;

import com.model.User;
import com.model.Users;

@Service
public class EmployeeDao {

	private static Users list = new Users();

	static {
		list.getUserList().add(new User("admin", "password", "admin@test.com", "colombo"));
		list.getUserList().add(new User("manager", "password", "admin@test.com", "colombo"));
		list.getUserList().add(new User("qa", "password", "admin@test.com", "colombo"));
	}

	public Users getAllUsers() {
		return list;
	}

	public void addUser(User user) {
		list.getUserList().add(user);
	}
}
